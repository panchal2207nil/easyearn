/**
 * Created by inno23 on 10/2/16.
 */
var FB = require('fb');
var request = require('request');
var constants = require('../utils/constant');
var _ = require('underscore');
var logger = require('../utils/logger');

var getUserFBProfile = function (access_token, callback) {
    FB.setAccessToken(access_token);
    FB.api('/me', 'GET', {"fields": "friends,id,birthday,first_name,last_name,name,gender,email,relationship_status,picture.height(500).width(500)"},
        function (response) {
            callback(response);
        }
    );
};

// For getting pages :  me/accounts?fields=picture.height(600).width(600),access_token,category,name,id

/**
 * @author - Neel
 * @modificationDate - 26/02/2016
 * @desc - To like post on fb
 * @param {Object} data - input data
 * @param callback - callback url
 */
var postLike = function (data, callback) {

    // To get post id
    var postID = _.where(data.offerDetails.offer_actions, {
        offerID: data.offerID,
        actionID: data.actionID
    })[0].postID;
    var url = constants.FB_POST_LIKE_URL.replace('<post_id>', postID);
    var method = 'POST';

    // method = 'POST'; // To like
    // method = 'DELETE'; // To Unlike

    logger.info('Post URL : ' + url);
    var params = {
        access_token: data.userFBdetails.accessToken
    };
    request({url: url, method: method, qs: params}, function (err, resp, body) {
        if (err) {
            logger.info('FB post like error :  ' + JSON.stringify(err));
            callback(err, null)
        } else {
            if (resp.statusCode == constants.STATUS_CODE.SUCCESS) {
                logger.info('FB post like response : ' + JSON.stringify(resp));
                logger.info('FB post like response body : ' + JSON.stringify(body));
                callback(null, resp);
            } else {
                logger.info(resp.statusCode);
                logger.info('FB post like response : ' + JSON.stringify(resp));
                logger.info('FB post like response body : ' + JSON.stringify(body));
                callback(resp, null);
            }

        }
    });

};


/**
 * @author - Neel
 * @modificationDate - 26/02/2016
 * @desc - To share page on facebook
 * @param {Object} data - input data
 * @param callback - callback url
 */

var pageShare = function (data, callback) {
    var pageID = _.where(data.offerDetails.offer_actions, {
        offerID: data.offerID,
        actionID: data.actionID
    })[0].pageID;

    var url = constants.FB_GRAPH_API_URL;
    var pageLink = constants.FB_PAGE_SHARE_URL.replace('<page_id>', pageID)
    var params = {
        access_token: data.userFBdetails.accessToken,
        message:data.recommendedText,
        link: pageLink
    };
    request.post({url: url, qs: params}, function (err, resp, body) {
        if (err) {
            logger.info('FB page share error :  ' + JSON.stringify(err));
            callback(err, null)
        } else {
            if (resp.statusCode == constants.STATUS_CODE.SUCCESS) {
                logger.info(resp.statusCode);
                logger.info('FB page share response : ' + JSON.stringify(resp));
                logger.info('FB page share response body : ' + JSON.stringify(body));
                callback(null, resp);
            } else {
                logger.info(resp.statusCode);
                logger.info('FB page share response : ' + JSON.stringify(resp));
                logger.info('FB page share response body : ' + JSON.stringify(body));
                callback(resp, null);
            }
        }
    });
};


/**
 * @author - Neel
 * @modificationDate - 26/02/2016
 * @desc - To share post on facebook
 * @param {Object} data - input data
 * @param callback - callback url
 */

var postShare = function (data, callback) {

    var postData = _.where(data.offerDetails.offer_actions, {
        offerID: data.offerID,
        actionID: data.actionID
    })[0].postID.split('_');

    var method = 'POST';
    var link = constants.FB_URL + postData[0] + "/posts/" + postData[1];
    var url = 'https://graph.facebook.com/me/feed';
    var params = {
        access_token: data.userFBdetails.accessToken,
        message: data.recommendedText,
        link: link
    };
        console.log('Param : ' + params);
    request.post({url: url, method: method, qs: params}, function (err, resp, body) {
        if (err) {
            logger.info('FB page share error :  ' + JSON.stringify(err));
            callback(err, null)
        } else {
            if (resp.statusCode == constants.STATUS_CODE.SUCCESS) {
                logger.info(resp.statusCode);
                logger.info('FB page share response : ' + JSON.stringify(resp));
                logger.info('FB page share response body : ' + JSON.stringify(body));
                callback(null, resp);
            } else {
                logger.info(resp.statusCode);
                logger.info('FB page share response : ' + JSON.stringify(resp));
                logger.info('FB page share response body : ' + JSON.stringify(body));
                callback(resp, null);
            }
        }
    });
};

var isUserUnlikedPage = function (pageDetails, callback) {
    FB.setAccessToken(pageDetails.accessToken);
    var query = "me/likes/" + pageDetails.pageID
    FB.api(query, function (response) {
        if (response.data.length === 1) { //there should only be a single value inside "data"
            callback(true);
        } else {
            callback(false);
        }
    });
};


var searchFbGroups = function (data, callback) {
    FB.setAccessToken(data.fbAccessToken);
    logger.info(new Date());
    FB.api(
        '/search',
        'GET',
        {"q": data.groupName, "type": "group", "fields": "icon,cover,description,name,members.summary(1).limit(0)"},
        function (response) {
            logger.info(JSON.stringify(response));
            logger.info(new Date());
            if (response) {
                callback(null, response.data);
            } else {
                callback('Error while getting facebook groups', null);
            }
        }
    );
}


/**
 * @author - Neel
 * @modificationDate - 26/02/2016
 * @desc - To share post on facebook
 * @param {Object} data - input data
 * @param callback - callback url
 */

var groupPost = function (accessToken, groupID, text, callback) {
    logger.info('accessToken  : ' + accessToken);
    logger.info('groupID  : ' + groupID);
    FB.setAccessToken(accessToken);
    var postUrl = "/" + groupID + "/feed"
    FB.api(
        postUrl,
        "POST",
        {
            "message": text
        },
        function (response) {
            logger.info("Response  : " + JSON.stringify(response));
            if (response && !response.error) {
                logger.info('Successfully posted in group URL ' + postUrl);
                callback(null, response);
            } else {
                logger.info('Error occur while posting in group URL ' + postUrl);
                callback(response, null)
            }
        });
};

var comment = function (data, callback) {

    logger.info(JSON.stringify(data));
    var post_id = _.where(data.offerDetails.offer_actions, {
        offerID: data.offerID,
        actionID: data.actionID
    })[0].postID;
    console.log(JSON.stringify(data));
    var query = "/" + post_id + "/comments"
    FB.setAccessToken(data.userFBdetails.accessToken);
    FB.api(
        query,
        "POST",
        {
            "message": data.recommendedText
        },
        function (response) {
            logger.info("Commented Response  : " + JSON.stringify(response));
            if (response && !response.error) {
                logger.info('Successfully commented on post ' + response);
                callback(null, response);
            } else {
                logger.info('Error occur while commenting on post ' + response);
                callback(response, null)
            }
        });
};

module.exports = {
    getUserFBProfile: getUserFBProfile,
    postLike: postLike,
    pageShare: pageShare,
    postShare: postShare,
    isUserUnlikedPage: isUserUnlikedPage,
    searchFbGroups: searchFbGroups,
    groupPost: groupPost,
    comment: comment
};
