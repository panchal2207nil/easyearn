var constant = require('../utils/constant');
/*var config = require('../config.json');*/
var fb = require('../datamodels/fb');
var languageMsg = require('../utils/languageMsg.js');
var userModel = require('../datamodels/userModel');
var _ = require('underscore');
var validator = require('../utils/validator');
var logger = require('../utils/logger');
var crypto = require('crypto');
var common = require('../datamodels/common');
var moment = require('moment');
var async = require("async");
var twitter = require("../controller/twitter");

/**
 * @author Neel
 * @Modificationdate : 10 Feb 2016
 * @desc To check facebook account is already regestered
 * @param {Object} data - input data JSON object
 * @param {string} data.access_token - access token of fabebook
 * @param callback - callback function
 */
var validateFBaccount = function (data, callback) {
    var responseObj = {};
    fb.getUserFBProfile(data.accessToken, function (response) {
        if (!response || response.error) {
            responseObj.statusCode = 400;
            responseObj.status = false;
            responseObj.isRegistered = false;
            responseObj.message = languageMsg.FB.INVALID_ACCESSTOKEN;
            logger.error('controller user validateFBaccount while getting facebook user data');
            callback(responseObj);
        } else {
            userModel.checkUsrExists(response.id, function (err, userData) {
                if (err) {
                    var totalFriends = response.friends.summary.total_count;
                    if (totalFriends < constant.TotalFriendCount) {
                        responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                        responseObj.status = false;
                        responseObj.isRegistered = false;
                        responseObj.message = languageMsg.FB.USER_FRIEND_VALIDATION;
                        logger.error('controller user validateFBaccount user does not met minimum friends condition');
                        callback(responseObj);
                    } else {
                        responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                        responseObj.status = true;
                        responseObj.isRegistered = false;
                        responseObj.firstName = response.first_name;
                        responseObj.lastName = response.last_name;
                        responseObj.picture = response.picture.data.url;
                        responseObj.message = languageMsg.FB.USER_NOT_REGISTERED;
                        logger.error('controller user validateFBaccount user is not registered');
                        callback(responseObj);
                    }
                } else {
                    data.user = userData;

                    updateAccessToken(data, function (result) {
                        callback(result);
                    });
                }
            });
        }
    });
};

/**
 * @author Harshad Parmar
 * @param {Object} data - input data JSON object
 * @param {string} data.phone - Mobile number for User registration
 * @param {string} data.country - Country for mobile number validation
 * @param callback
 */
var validateUserProfile = function (data, callback) {
    var responseObj = {};
    var isValid = validator.validateUserProfile(data); //validation for user profile
    if (isValid === true) {
        fb.getUserFBProfile(data.accessToken, function (fbData) {
            if (!fbData || fbData.error) {
                responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                responseObj.status = false;
                responseObj.message = languageMsg.FB.INVALID_ACCESSTOKEN;
                callback(responseObj);
                logger.error('controller user validateUserProfile fb getUserFBProfile error ' + JSON.stringify(fbData));
            } else {
                data.fb = fbData;
                userModel.getUserFromMobile(data, function (err_user, result_user) {
                    if (result_user) {
                        responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                        responseObj.status = false;
                        responseObj.message = languageMsg.USER.MOBILE_EXIST;
                        logger.error('controller user validateUserProfile userModel getUserFromMobile error ' + JSON.stringify(result_user));
                        callback(responseObj);
                    } else {
                        storeUserData(data, function (response) {
                            callback(response);
                        });
                    }
                });
            }
        });
    } else {
        responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
        responseObj.status = false;
        responseObj.message = languageMsg.USER.VALIDATE_USER_PROFILE;
        callback(responseObj);
        logger.error('controller user validateUserProfile error ' + isValid);
    }
};


/**
 * @author Harshad Parmar
 * @param {Object} data - input data JSON object
 * @param callback
 */
var storeUserData = function (data, callback) {
    var responseObj = {};
    userModel.extendFBToken(data.accessToken, function (extendedToken) {
        if (extendedToken) {
            var token = crypto.randomBytes(15).toString('hex');
            var userObj = {
                mobileNo: data.phone.trim(),
                countryCode: data.country,
                userType: constant.USER_TYPE.END_USER,
                accessToken: token
            };
            userModel.insertUserData(userObj, function (err, user_data) {
                if (err) {
                    responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                    responseObj.status = false;
                    responseObj.message = err;
                    callback(responseObj);
                    logger.error('controller user storeUserData userModel insertUserData error ' + err);
                } else {
                    data.user = user_data;
                    data.sourceID = constant.SOURCE.FACEBOOK;
                    if (extendedToken.expires) {
                        data.tokenExpiredDate = moment().seconds(moment().seconds() + extendedToken.expires).format("YYYY-MM-DD HH:mm:ss");
                    } else {
                        data.tokenExpiredDate = moment().add(constant.FB_TOKEN_EXPIRED_MONTHS, 'months').format("YYYY-MM-DD HH:mm:ss");
                    }
                    var currentDate = moment().format("YYYY-MM-DD HH:mm:ss");
                    var sourceObj = {
                        userID: user_data.userID,
                        sourceID: data.sourceID,
                        socialID: data.fb.id,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        emailID: data.fb.email,
                        accessToken: extendedToken.access_token,
                        refreshToken: '',
                        tokenExpiredDate: data.tokenExpiredDate,
                        updateDate: currentDate,
                        connectedDate: currentDate
                    };

                    userModel.insertAccessToken(sourceObj, function (err_token, result_token) {
                        if (err_token) {
                            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                            responseObj.status = false;
                            responseObj.message = err;
                            logger.error('controller user storeUserData userModel insertAccessToken error ' + err_token);
                            callback(responseObj);
                        } else {
                            userModel.createFBUser(data, function (err_fb, result_fb) {
                                if (err_fb) {
                                    responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                                    responseObj.status = false;
                                    responseObj.message = languageMsg.ERROR;
                                    logger.error('controller user storeUserData userModel createFBUser error ' + err_token);
                                    callback(responseObj);
                                } else {
                                    responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                                    responseObj.status = true;
                                    responseObj.data = {
                                        accessToken: token,
                                        userID: user_data.userID
                                    };
                                    responseObj.message = languageMsg.USER.USER_CREATED_SUCCESS;
                                    logger.info('controller user storeUserData userModel createFBUser exit');
                                    callback(responseObj);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = false;
            responseObj.message = languageMsg.ERROR;
            logger.error('controller user storeUserData error ');
            callback(responseObj);
        }
    });
};

/**
 *
 * @author - Neel
 * @modificationDate - 18/02/2016
 * @desc - To get offer list
 * @param {Object} data - input data JSON object
 * @param {string} data.accessToken - accessToken to validate user
 * @param {string} data.startIndex - start index for offer listing pagination
 * @param {Object} data.user - user data
 * @param callback
 */

var getOfferList = function (data, callback) {
    var responseObj = {};
    // To get user connected sources
    userModel.getConnectedSource(data.user.userID, function (err, sources) {
        if (sources.length == 0) {
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = false;
            responseObj.message = languageMsg.USER.CONNECT_SOURCE;
            logger.error('controller user getOfferList : User has not connected to any source');
            callback(responseObj);
        } else {
            data.sources = sources;
            userModel.getUserOffers(data, function (err, userOffers) {
                var userAcceptedOffer = createUserAcceptedObject(userOffers);
                data.offersToRemove = userAcceptedOffer.offersToRemove;
                // To get offer list based on connected sources
                userModel.getOfferList(data, function (err, offerList) {
                    if (err) {
                        responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                        responseObj.status = false;
                        responseObj.message = languageMsg.OFFER.OFFER_LISTING_ERROR;
                        logger.error('controller user getOfferList : Error occur while getting offer list' + err);
                        callback(responseObj);
                    } else {
                        responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                        responseObj.status = true;
                        if (offerList.length < constant.LIST_VIEW.START_COUNT) {
                            responseObj.hasMoreData = false;
                        } else {
                            responseObj.hasMoreData = true;
                        }

                        // Parse sequelize object to json
                        offerList = JSON.parse(JSON.stringify(offerList));

                        responseObj.count = offerList.length;

                        // Logic to add action status
                        for (var offerIndex in offerList) {
                            var offer_actions = offerList[offerIndex].offer_actions;
                            for (var actionIndex  in offer_actions) {
                                var actions = {};
                                actions = offer_actions[actionIndex];
                                var peformActionStatus = _.where(userAcceptedOffer.usersAcceptedOffers, {
                                    offerID: offerList[offerIndex].offerID,
                                    actionID: actions.actionID
                                });

                                if (peformActionStatus.length == 0) {
                                    actions.actualCount = 0;
                                } else {
                                    actions.actualCount = peformActionStatus[0].actualCount;
                                }
                            }

                        }
                        responseObj.coins = data.user.coins;
                        responseObj.result = offerList;

                        logger.info('controller user getOfferList : Successfully get offer list');
                        callback(responseObj);
                    }
                });
            });
        }
    });
};

/**
 * @author Neel
 * @desc To create object of user accepted offer and To get list of offers to remove
 * @param usersAcceptedOffers
 * @returns {{}} Object
 */

var createUserAcceptedObject = function (usersAcceptedOffers) {
    var offers = [];
    var offerIDs = [];
    var offersToRemove = [];
    var offersActionPerformed
    for (var index in usersAcceptedOffers) {
        var offerID = usersAcceptedOffers[index].offerID;
        var offerDetails = usersAcceptedOffers[index];
        if (offerIDs.indexOf(offerID) == -1) {
            var offer = {}
            offer.offerID = offerDetails.offerID;
            offer.actions = offerDetails.offer.offer_actions;
            var offerPerformedActions = [];
            var action = {};
            action.offerID = offerDetails.offerID;
            action.actionID = offerDetails.actionID;
            action.coins = offerDetails.coins;
            action.attemptCount = offerDetails.attemptCount;
            action.actualCount = offerDetails.actualCount;
            offerPerformedActions.push(action);
            offer.offerPerformedActions = offerPerformedActions;
            offers.push(offer);
            offerIDs.push(offerID);
            if (_.where(offer.offerPerformedActions, {actualCount: 1}).length == offer.actions.length) {
                offersToRemove.push(offerDetails.offerID);
            }
        } else {
            var offer = _.where(offers, {offerID: offerID})[0];
            var action = {};
            action.offerID = offerDetails.offerID;
            action.actionID = offerDetails.actionID;
            action.coins = offerDetails.coins;
            action.attemptCount = offerDetails.attemptCount;
            action.actualCount = offerDetails.actualCount;
            offer.offerPerformedActions.push(action);
            if (_.where(offer.offerPerformedActions, {actualCount: 1}).length == offer.actions.length) {
                offersToRemove.push(offerDetails.offerID);
            }
        }

    }

    var offerList = {};
    offerList.offersToRemove = offersToRemove;
    offerList.offers = offers;
    offerList.usersAcceptedOffers = usersAcceptedOffers;
    return offerList;
}

/**
 * @author Neel
 * @Modificationdate : 18 Feb 2016
 * @desc To create or update earnEazy access token and To update FB access Token
 * @param {Object} data - input data JSON object
 * @param {string} data.access_token - access token of fabebook
 * @param {Object} data.user -  user data
 * @param callback - callback function
 */
var updateAccessToken = function (data, callback) {
    var responseObj = {};

    // To Create / Update earnEazy access token
    common.updateAccessToken(data.user.userID, function (err, accessToken) {
        if (err) {
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = true;
            responseObj.isRegistered = true;
            responseObj.message = languageMsg.ERROR;
            logger.error('controller user validateFBaccount : error in updating access token');
            callback(responseObj);
        } else {
            responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
            responseObj.status = true;
            responseObj.isRegistered = true;
            responseObj.accessToken = accessToken;
            responseObj.message = languageMsg.USER.LOGIN_SUCCESS;

            // To get coins
            userModel.getUserData(data, function (err, userDetails) {
                if (err) {
                    responseObj.coins = '';
                } else {
                    responseObj.coins = userDetails.coins;
                    responseObj.userID = data.user.userID;
                    responseObj.firstName = data.user.firstName;
                    responseObj.lastName = data.user.lastName;
                    responseObj.emailID = data.user.emailID;
                    responseObj.mobileNo = userDetails.mobileNo;
                }
                logger.info('controller user validateFBaccount user already registered');
                callback(responseObj);
            });
        }

        // Background function : To extend FB token and store it in to DB
        // To get FB extended access token
        userModel.extendFBToken(data.accessToken, function (extendedToken) {
            logger.info('controller updateAccessToken extendFBToken : ' + JSON.stringify(extendedToken));
            if (extendedToken) {
                data.extendedToken = extendedToken.access_token;
                if (extendedToken.expires) {
                    data.tokenExpiredDate = moment().seconds(moment().seconds() + extendedToken.expires).format("YYYY-MM-DD HH:mm:ss");
                } else {
                    data.tokenExpiredDate = moment().add(constant.FB_TOKEN_EXPIRED_MONTHS, 'months').format("YYYY-MM-DD HH:mm:ss");
                }
                data.updatedDate = moment().seconds(moment().seconds()).format("YYYY-MM-DD HH:mm:ss");

                // To update FB access token in Db
                common.updateFBtoken(data, function (err, result) {
                    if (err) {
                        logger.error('controller user updateAccessToken : Error occur while updating user FB token');
                    } else {
                        logger.info('controller user updateAccessToken : User FB token updated successfully');
                    }
                });
            } else {
                logger.error('user updateAccessToken : Error occur while getting FB extended token');
            }
        });
    });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc : To perform action
 * @param {Object} data - input data JSON object
 * @param {Object} data.user - user data based on access token
 * @param callback - callback function
 */
var verifyAction = function (data, callback) {
    var responseObj = {};

    data.offerID = parseInt(data.offerID);
    data.actionID = parseInt(data.actionID);
    data.actualCount = parseInt(data.actualCount);

    // To get user regeistered FB account
    userModel.getUserFbDetails(data.fbID, function (err, userFBdetails) {

        logger.info('userFBdetails : ' + JSON.stringify(userFBdetails));
        logger.info('data : ' + JSON.stringify(data));
        if (!userFBdetails) {
            // If user perform action other then registered Fb id then user will not get coins
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.message = languageMsg.ACTIONS.INVALID_FB_ACCOUNT;
            responseObj.status = false;
            logger.error('user performAction : ' + languageMsg.ACTIONS.INVALID_FB_ACCOUNT);
            callback(responseObj);
        } else {
            data.userFBdetails = userFBdetails;
            // To get offer details
            userModel.getOffferDetails(data, function (err, offerDetails) {
                if (offerDetails && offerDetails.length != 0) {
                    data.offerDetails = offerDetails[0];
                    data.totalOfferActions = offerDetails[0].offer_actions.length;
                    var performAction = _.find(data.offerDetails.offer_actions, {actionID: data.actionID});
                    // To check action is associated with offer ot not
                    if (!performAction) {
                        // offer or action not found
                        responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                        responseObj.message = languageMsg.INVALID_REQUEST;
                        responseObj.status = false;
                        logger.error('user performAction : ' + languageMsg.INVALID_REQUEST);
                        callback(responseObj);
                    } else {
                        console.log('data : ' + JSON.stringify(data));
                        data.performAction = performAction;
                        var actionCoins = performAction.coins;
                        var merchantCoins = data.offerDetails.admin.coins;
                        if (merchantCoins < actionCoins) {
                            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                            responseObj.status = false;
                            responseObj.message = languageMsg.ACTIONS.MERCHANT_INSUFFICIENT_BALANCE;
                            logger.error('user performAction : ' + languageMsg.ACTIONS.MERCHANT_INSUFFICIENT_BALANCE);
                            callback(responseObj);
                        } else {
                            // Perform FB or twitter actoion
                            performActions(data, function (result) {
                                callback(result);
                            });
                        }
                    }
                }
                else {
                    // Invalid offer id
                    responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                    responseObj.message = languageMsg.INVALID_REQUEST;
                    responseObj.status = false;
                    logger.error('user performAction : invalid offer id ' + languageMsg.INVALID_REQUEST);
                    callback(responseObj);
                }
            });
        }
    });
};

var performActions = function (data, callback) {

    if (data.actionID == constant.ACTIONS.POST_LIKE) {
        // To perform facebook post like
        fb.postLike(data, function (err, result) {
            if (err) {
                logger.error('Error while liking post' + JSON.stringify(data));
                data.actualCount = 0;
            }
            else {
                logger.info('Post like successfully');
                data.actualCount = 1;
            }
            data.fbActionResult = err;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.PAGE_SHARE) {

        // To share facebook page
        fb.pageShare(data, function (err, result) {
            if (err) {
                logger.error('Error while sharing page' + JSON.stringify(data));
                data.actualCount = 0;
            }
            else {
                logger.info('Page share successfully');
                data.actualCount = 1;
            }
            data.fbActionResult = err;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.POST_SHARE) {

        // To share post on facebook
        fb.postShare(data, function (err, result) {
            if (err) {
                logger.error('Error while sharing post' + JSON.stringify(data));
                data.actualCount = 0;
            }
            else {
                logger.info('Post share successfully');
                data.actualCount = 1;
            }
            data.fbActionResult = err;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.PAGE_LIKE) {
        // To update user action details
        updateUserActionDetails(data, function (result) {
            callback(result);
        });
    } else if (data.actionID == constant.ACTIONS.TWEET_LIKE) {
        twitter.like_tweet(data, function (err, tweet) {
            var fbActionResult = {};
            if (err) {
                logger.error('Error while liking tweet' + JSON.stringify(err));
                data.actualCount = 0;
                fbActionResult.statusCode = constant.STATUS_CODE.BAD_REQUEST;

            } else {
                logger.info('Tweet liked successfully' + JSON.stringify(tweet));
                data.actualCount = 1;
                fbActionResult.statusCode = constant.STATUS_CODE.SUCCESS;
            }
            data.fbActionResult = fbActionResult;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.RE_TWEET) {
        twitter.reTweet(data, function (err, tweet) {
            var fbActionResult = {};
            if (err) {
                logger.error('Error while ReTweet' + JSON.stringify(err));
                data.actualCount = 0;
                fbActionResult.statusCode = constant.STATUS_CODE.BAD_REQUEST;

            } else {
                logger.info('Re Tweet successfully' + JSON.stringify(tweet));
                data.actualCount = 1;
                fbActionResult.statusCode = constant.STATUS_CODE.SUCCESS;
            }
            data.fbActionResult = fbActionResult;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.FOLLOW) {
        twitter.follow(data, function (err, tweet) {
            var fbActionResult = {};
            if (err) {
                logger.error('Error while Following user' + JSON.stringify(err));
                data.actualCount = 0;
                fbActionResult.statusCode = constant.STATUS_CODE.BAD_REQUEST;

            } else {
                logger.info('User Follow successfully' + JSON.stringify(tweet));
                data.actualCount = 1;
                fbActionResult.statusCode = constant.STATUS_CODE.SUCCESS;
            }
            data.fbActionResult = fbActionResult;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });
        });
    } else if (data.actionID == constant.ACTIONS.GROUP_POST) {

        console.log('Data : ' + JSON.stringify(data));
        logger.info(JSON.stringify(data));
        var accessToken = data.userFBdetails.accessToken;
        var textToPost = data.recommendedText + ' \n  ' + data.offerDetails.postMessage;
        logger.info('text  : ' + textToPost);
        console.log('text  : ' + textToPost);
        var groups = data.groups;
        var successGroup = [];
        var failGroup = [];
        async.forEachLimit(data.groups, 1, function (group, callback) {
            // To group post
            fb.groupPost(accessToken, group.groupID, textToPost, function (err, result) {
                if (err) {
                    logger.error('Error while posting in group : ' + JSON.stringify(group));
                    //data.actualCount = 0;
                    failGroup.push(group);
                }
                else {
                    logger.info('Post in group successfully' + group.id);
                    data.actualCount = 1;
                    successGroup.push(group);
                }
                data.fbActionResult = err;
                callback();
            });
        }, function (err) {
            logger.info('Success Group  : ' + JSON.stringify(successGroup));
            logger.info('Fail Group  : ' + JSON.stringify(failGroup));
            if (err) {
                callback({status: "Error"});
            } else {
                data.successGroup = successGroup;
                // If user has not post successfully in any group
                // then again show the offer
                if (data.successGroup.length == 0) {
                    data.actualCount = 0;
                }
                // To update user action details
                updateUserActionDetails(data, function (result) {
                    result.failGroup = failGroup;
                    result.successGroup = successGroup;
                    callback(result);
                });

                if (data.successGroup.length > 0) {
                    addGroups(data, function (err, result) {
                        if (err) {
                            logger.info('Error while adding group');
                        } else {
                            logger.info('Group added successfully');
                        }
                    });
                }
            }
        });
    } else if (data.actionID == constant.ACTIONS.COMMENT) {
        // To comment post on facebook
        fb.comment(data, function (err, result) {
            if (err) {
                logger.error('Error while posting in group : ' + JSON.stringify(data));
                data.actualCount = 0;
            }
            else {
                logger.info('Post in group successfully');
                data.actualCount = 1;
            }
            data.fbActionResult = err;
            // To update user action details
            updateUserActionDetails(data, function (result) {
                callback(result);
            });


        });
    }
    else {
        // To update user action details
        updateUserActionDetails(data, function (result) {
            callback(result);
        });
    }
};


/**
 * @author Neel
 * @Modificationdate : 26 Feb 2016
 * @desc : To update user action details
 * @param {Object} data - input data JSON object
 * @param callback - callback function
 */

var updateUserActionDetails = function (data, callback) {
    var responseObj = {}, coins;
    // Get user performed actions of offer
    userModel.getUserAction(data, function (err, userActionDetails) {

        // if userActionDetails == null then add user action
        // else update user action

        if (userActionDetails == null) {
            logger.info('user performAction : add user action');
            // add user action
            var user_action_data = {};
            user_action_data.userID = data.user.userID;
            user_action_data.offerActionID = data.performAction.offerActionID;
            user_action_data.attemptCount = 1;
            user_action_data.actualCount = data.actualCount;
            user_action_data.transactionDate = moment().seconds(moment().seconds()).format("YYYY-MM-DD HH:mm:ss");
            user_action_data.offerID = data.offerID;
            user_action_data.actionID = data.actionID;
            user_action_data.isUnliked = constant.isUnliked.FALSE;

            coins = data.performAction.coins;
            if (data.actionID == constant.ACTIONS.GROUP_POST) {
                coins = coins * data.successGroup.length;
            }
            user_action_data.coins = coins;

            userModel.addUserAction(user_action_data, function (err, result) {
                // Perform transactions if actual count is 1
                if (data.actualCount == constant.ACTION_STATUS.SUCCESS) {
                    // Deduct coins from merchant account
                    userModel.deduct_coins(coins, data.offerDetails.admin.id, function (err, merchantDetails) {
                        logger.info('user performAction : coins deducted from merchant account successfully');
                        // Increase coins from merchant account
                        userModel.increase_coins(coins, data.user.userID, function (err, userDetails) {
                            logger.info('user performAction : coins increse in to user account successfully');
                            // To check user has perform all actions
                            checkAllActionsPerformed(data, function (hasPerformedAllActions) {
                                logger.info('user performAction : checkAllActionsPerformed peformed successfully');
                                responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                                responseObj.status = true;
                                responseObj.coins = data.user.coins + coins;
                                responseObj.hasPerformedAllActions = hasPerformedAllActions;
                                callback(responseObj);
                            });
                        });
                    });
                } else {
                    // Only add user actions if actual count is 0
                    responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                    responseObj.status = false;
                    responseObj.message = languageMsg.ACTIONS.PERFORM_ACTION_ERROR;
                    responseObj.coins = data.user.coins
                    logger.info('user performAction : user action added successfully with actual count 0');
                    callback(responseObj);
                }
            });
        } else {
            logger.info('user performAction : update user action');
            // update user action
            if (userActionDetails.actualCount == constant.ACTION_STATUS.SUCCESS) {
                // Restrict user if user has already perform action
                responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                responseObj.status = false;
                responseObj.message = languageMsg.ACTIONS.USER_ALREADY_PERFORM_ACTION;
                callback(responseObj);
            } else {
                data.userActionDetails = userActionDetails;
                // Update user action data
                userModel.updateUserAction(data, function (err, result) {
                    if (data.actualCount == constant.ACTION_STATUS.SUCCESS) {
                        coins = data.performAction.coins;
                        if (data.actionID == constant.ACTIONS.GROUP_POST) {
                            coins = coins * data.successGroup.length;
                        }
                        // Deduct coins from merchant account
                        userModel.deduct_coins(coins, data.offerDetails.admin.id, function (err, merchantDetails) {
                            logger.info('user performAction : coins deducted from merchant account successfully');
                            // Increase coins from merchant account
                            userModel.increase_coins(coins, data.user.userID, function (err, userDetails) {
                                logger.info('user performAction : coins increse in to user account successfully');
                                // To check user has perform all actions
                                checkAllActionsPerformed(data, function (hasPerformedAllActions) {
                                    responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                                    responseObj.status = true;
                                    responseObj.coins = data.user.coins + coins;
                                    responseObj.message = languageMsg.ACTIONS.ACTION_SUCCESS;
                                    responseObj.hasPerformedAllActions = hasPerformedAllActions;
                                    callback(responseObj);
                                });

                            });
                        });
                    } else {
                        logger.info('actual count 0' + JSON.stringify(data.fbActionResult));
                        if (data.fbActionResult.statusCode != constant.STATUS_CODE.SUCCESS) {
                            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
                            responseObj.status = false;
                            // Change response message only when there is group post
                            if (data.actionID == constant.ACTIONS.GROUP_POST) {
                                responseObj.message = languageMsg.ACTIONS.GROUP_POST_PERMISSION_ERROR;
                            } else {
                                responseObj.message = languageMsg.ACTIONS.PERFORM_ACTION_ERROR;
                            }
                            responseObj.coins = data.user.coins;
                            logger.info('user performAction : Error occur while performing action');
                            callback(responseObj);
                        } else {
                            // Only update user actions if actual count is 0
                            responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
                            responseObj.status = true;
                            responseObj.message = languageMsg.ACTIONS.USER_ALREADY_PERFORM_ACTION;
                            responseObj.coins = data.user.coins;
                            logger.info('user performAction : user action updated successfully');
                            callback(responseObj);
                        }
                    }
                });// End of updateUserAction
            }
        }
    });// End of getUserAction
}

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc : To check user has perform all actions of perticular offer
 * @param data - Input data
 * @param callback - callback function
 */

var checkAllActionsPerformed = function (data, callback) {
    logger.info('user checkAllActionsPerformed enter ');
    userModel.getUserPerformedAction(data, function (err, userActions) {
        var completedAllAction = 0;
        for (var index in userActions) {
            if (userActions[index].actualCount == 1) {
                completedAllAction++;
            }
        }
        if (completedAllAction == data.totalOfferActions) {
            logger.info('user checkAllActionsPerformed : completed all actions ');
            callback(true);
        } else {
            logger.info('user checkAllActionsPerformed : some actions remaining');
            callback(false);
        }
    });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc : To get user transaction
 * @param data - Input data
 * @param callback - callback function
 */
var getTransactions = function (data, callback) {
    var responseObj = {};
    userModel.getUserTransactions(data, function (err, userTransactions) {
        if (err) {
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = false;
            responseObj.message = languageMsg.TRANSACTION_LIST.TRANSACTION_LIST_ERROR;
            logger.error('user getTransactions :  ' + languageMsg.TRANSACTION_LIST.TRANSACTION_LIST_ERROR);
            callback(responseObj);
        } else {
            responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
            responseObj.status = true;
            if (userTransactions.length < constant.LIST_VIEW.START_COUNT) {
                responseObj.hasMoreData = false;
            } else {
                responseObj.hasMoreData = true;
            }
            responseObj.result = userTransactions;
            logger.info('user getTransactions : ' + languageMsg.TRANSACTION_LIST.TRANSACTION_LIST_SUCCESS);
            callback(responseObj);
        }
    });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc : To get user has unliked page or not
 * @param data - Input data
 * @param callback - callback function
 */
var checkUserLikes = function (data, callback) {
    var fbID = data.entry[0].uid;
    // To get user liked pages
    userModel.getUserLikedPages(fbID, function (err, userLikedPages) {
        async.forEachLimit(userLikedPages, 1, function (likedPageDetails, callback) {
            // To check user has unliked fb page by calling fb api
            fb.isUserUnlikedPage(likedPageDetails, function (isLiked) {
                // If page is still liked then do nothing
                // Else deduct money from user account and add it in merchant account
                if (isLiked) {
                    logger.info(likedPageDetails.objectID + ' : Page is already liked');
                    callback();
                } else {
                    // Deduct coins from user account
                    userModel.deduct_coins(likedPageDetails.coins, likedPageDetails.userID, function (err, merchantDetails) {
                        logger.info('user performAction : coins deducted from merchant account successfully');
                        // Increase coins in merchant account
                        userModel.increase_coins(likedPageDetails.coins, likedPageDetails.merchantID, function (err, userDetails) {
                            logger.info('user performAction : coins increse in to user account successfully');
                            // Change offer action status to pending mode
                            userModel.updateOfferActionStatus(likedPageDetails, function (err, result) {
                                if (err) {
                                    logger.info('user performAction : Error occur while changing user action status');
                                } else {
                                    logger.info('user performAction : user action status changed successfully');
                                }
                                callback();
                            });// End of updateOfferActionStatus
                        });// End of increase_coins
                    });// End of deduct_coins
                }
            });// End of isUserUnlikedPage
        }, function (err) {
            var statusObj = {};
            if (err) {
                logger.info('checkUserLikes : Error occur while checking user page likes');
                statusObj.status = 'Fail';
                callback();
            } else {
                logger.info('checkUserLikes : User likes checked successfully');
                statusObj.status = 'Success';
                callback(statusObj);
            }
        });// End of async.forEachLimit
    });// End of getUserLikedPages
};

/**
 * @author Neel
 * @Modificationdate : 21 Jun 2016
 * @desc : To search groups from facebok
 * @param data - Input data
 * @param callback - callback function
 */

var findGroups = function (data, callback) {
    // Get user details
    userModel.getUserFbData(data.user.userID, function (err, fbToken) {
        data.fbAccessToken = fbToken[0].accessToken;
        // search group in facebook
        fb.searchFbGroups(data, function (err, groups) {
            if (err) {
                logger.error(languageMsg.FB.SEARCH_GROUP_ERROR);
                callback('Error while getting facebook groups', null);
            } else {
                logger.info('Successfully get facebook groups' + JSON.stringify(groups));
                var groupList = {};
                groupList.groups = groups;
                callback(null, groupList);
            }
        })
    })
};

/**
 * @author Neel
 * @Modificationdate : 21 Jun 2016
 * @desc : To add user groups
 * @param data - Input data
 * @param callback - callback function
 */
var addGroups = function (data, callback) {
    var subQuery = '', cover = '', responseObj = {};
    var query = 'MATCH (u:User{userID : "' + data.user.userID + '" }) '
    var groups = data.successGroup;
    for (var i = 0; i < groups.length; i++) {
        var desc = '';
        if (groups[i].description) {
            desc = groups[i].description.replace(/\n/g, ' ').replace(/'/g, '');
        }
        if (groups[i].cover) {
            cover = groups[i].cover
        }
        subQuery += " MERGE (grp" + i + ":Group { groupID : " + groups[i].groupID + "}) "
            + " ON CREATE SET grp" + i + ".name = '" + groups[i].name + "',"
            + " grp" + i + ".description = '" + desc + "', "
            + ' grp' + i + '.cover = "' + cover + '"'
            + ' merge (u)-[:JOINED]->(grp' + i + ')'
    }
    query += subQuery;
    logger.info('Add user group query  : ' + query);

    // Add user groups in neo4j
    userModel.addGroups(query, function (err, result) {
        if (err) {
            logger.error(languageMsg.USER.GROUP_ADD_ERROR + ' ' + JSON.stringify(err));
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = false;
            responseObj.message = languageMsg.USER.GROUP_ADD_ERROR;
            callback(responseObj, null);
        } else {
            logger.info(languageMsg.USER.GROUP_ADD_SUCCESS + ' ' + JSON.stringify(result));
            responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
            responseObj.status = true;
            responseObj.message = languageMsg.USER.GROUP_ADD_SUCCESS;
            callback(null, responseObj);
        }
    });
};


/**
 * @author Neel
 * @Modificationdate : 21 Jun 2016
 * @desc : To get user groups
 * @param data - Input data
 * @param callback - callback function
 */

var getUserGroups = function (data, callback) {
    var responseObj = {};
    var query = 'MATCH (n:User)-[j:JOINED]->(gp:Group) where n.userID = "' + data.user.userID + '"' +
        ' RETURN gp.cover as cover,gp.groupID as groupID,gp.name as name,gp.description as description';

    /*var query = 'MATCH (n:User)-[j:JOINED]->(gp:Group) where n.userID = ' + data.user.userID +
     ' RETURN gp.cover as cover,gp.groupID as id,gp.name as name,gp.description as description';*/
    logger.info('get user group query : ' + query);
    userModel.getUserGroups(query, function (err, result) {
        if (err) {
            logger.error(languageMsg.USER.GROUP_GET_ERROR + ' ' + JSON.stringify(err));
            responseObj.statusCode = constant.STATUS_CODE.BAD_REQUEST;
            responseObj.status = false;
            responseObj.message = languageMsg.ERROR;
            callback(responseObj, null);
        } else {
            logger.info(languageMsg.USER.GROUP_GET_SUCCESS + ' ' + JSON.stringify(result));
            responseObj.statusCode = constant.STATUS_CODE.SUCCESS;
            responseObj.status = true;
            responseObj.groups = result;
            callback(null, responseObj);
        }
    })

};

module.exports = {
    validateFBaccount: validateFBaccount,
    validateUserProfile: validateUserProfile,
    getOfferList: getOfferList,
    verifyAction: verifyAction,
    getTransactions: getTransactions,
    updateUserActionDetails: updateUserActionDetails,
    checkUserLikes: checkUserLikes,
    findGroups: findGroups,
    addGroups: addGroups,
    getUserGroups: getUserGroups
};