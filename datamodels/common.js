/**
 * Created by inno23 on 12/2/16.
 */

var languageMsg = require('../utils/languageMsg');
var modal = require("../datamodels/modal");
var logger = require('../utils/logger');
var constant = require('../utils/constant');
var crypto = require('crypto');
var moment = require('moment');

/**
 * @author Neel
 * @Modificationdate - 18 Feb 2016
 * @desc - To update user earnEazy access token
 * @param {string} userID - User id for updating user token
 * @param callback - callback function
 */
var updateAccessToken = function (userID, callback) {
    var updatedAccessToken = userID + '' + moment() + '' + constant.ACCESSTOKENLIMIT;
    updatedAccessToken = crypto.createHash('md5').update(updatedAccessToken).digest("hex");

    //crypto.randomBytes(15).toString('hex');
   /* modal.user
        .update({accessToken: updatedAccessToken}, {where: {userID: userID}})
        .then(function (result) {
            if (result === null) {
                logger.error('common updateAccessToken error : ' + result);
                callback(languageMsg.USER.USER_NOT_FOUND, result);
            } else {
                logger.info('common updateAccessToken result : ' + updatedAccessToken);
                console.log('validateFBaccount 10 : ' + moment().seconds(moment().seconds()).format("YYYY-MM-DD HH:mm:ss SSS"));
                callback(null, updatedAccessToken);
            }
        })
        .catch(function (exception) {
            logger.error('common updateAccessToken exception : ' + exception);
            callback(exception, null);
        });*/


    var query = "UPDATE users SET accessToken = '" + updatedAccessToken + "' WHERE userID = " + userID;
    modal.sequelize.query(query).then(function (result) {
        if (result === null) {
            logger.error('common updateAccessToken error : ' + result);
            callback(languageMsg.USER.USER_NOT_FOUND, result);
        } else {
            logger.info('common updateAccessToken result : ' + updatedAccessToken);
            console.log('validateFBaccount 10 : ' + moment().seconds(moment().seconds()).format("YYYY-MM-DD HH:mm:ss SSS"));
            callback(null, updatedAccessToken);
        }
    }).catch(function (exception) {
        logger.error('common updateAccessToken exception : ' + exception);
        callback(exception, null);
    });

};


/**
 * @author Neel
 * @Modificationdate - 18 Feb 2016
 * @desc - To update user Facebook access token
 * @param {Object} data - input data JSON object
 * @param {string} data.extendedToken - FB extended token
 * @param {date}  data.tokenExpiredDate - FB extended token expired date
 * @param callback - callback function
 */
var updateFBtoken = function (data, callback) {
    modal.user_sources
        .update({
            accessToken: data.extendedToken,
            tokenExpiredDate: data.tokenExpiredDate,
            updateDate: data.updatedDate
        }, {where: {userID: data.user.userID}})
        .then(function (result) {

            if (result === null) {
                logger.error('common updateFBtoken error : Error occur while updating Fb token' + result);
                callback(languageMsg.USER.USER_NOT_FOUND, result);
            } else {
                logger.info('common updateFBtoken success : User Fb extended token updated successfully' + result);
                callback(null, result);
            }
        })
        .catch(function (exception) {
            logger.error('common updateFBtoken exception : Exception occur while updating Fb token' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate - 18 Feb 2016
 * @desc To authenticate access token
 * @param {string} accessToken - earnEazy access token
 * @param callback - callback function
 */
var authenticateAccessToken = function (accessToken, callback) {
    modal.user.find({where: {accessToken: accessToken}}).then(function (result) {
        if (result === null) {
            callback(null, result);
        } else {
            callback(null, result);
        }
    });
};


/**
 * @author Neel
 * @ModificationDate - 12 Feb 2016
 * @desc To get total friends count from db, It Will replace constant.TotalFriendCount when server starts
 * @param callback - callback function
 */
var getTotalFriendCount = function (callback) {
    modal.sequelize
        .query('select TotalFriendCount from constant', {type: modal.sequelize.QueryTypes.SELECT})
        .then(function (result) {
            if (result) {
                logger.info('common getTotalFriendCount result ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.info('common getTotalFriendCount result exit');
                callback(languageMsg.COMMON.USER_NOT_REGISTERED, result);
            }
        })
        .catch(function (exception) {
            logger.info('common getTotalFriendCount result exception : ' + exception);
            callback(exception, null);
        });
};
/**
 * @author Neel
 * @desc - Calling of getTotalFriendCount function when server starts
 * @ModificationDate - 12 Feb 2016
 */

getTotalFriendCount(function (err, result) {
    if (result) {
        constant.TotalFriendCount = result[0].TotalFriendCount;
        console.log('TotalFriendCount : ' + constant.TotalFriendCount);
        logger.info('Total Friend Counts : ' + constant.TotalFriendCount);
    } else {
        console.log('Total Counterr ' + result);
        logger.error('businessModel getKoinsValue error ' + err);
    }
});


module.exports = {
    getTotalFriendCount: getTotalFriendCount,
    authenticateAccessToken: authenticateAccessToken,
    updateAccessToken: updateAccessToken,
    updateFBtoken: updateFBtoken
}
