/**
 * Created by inno41 on 9/2/16.
 */
var languageMsg = require('../utils/languageMsg.json');
var userController = require("../controller/user");
var parser = require('../routes/parser');
var constant = require('../utils/constant');
var home = function (req, res) {
    userController.home(req.body, function (result) {
        res.send(result);
    });
};
/**
 * Developer Name : Neel
 * Modification date : 10 Feb 2016
 *
 * To validate FB account
 * @param req
 *           access_token : access token of fabebook
 * @param res
 */
var validateFBaccount = function (req, res) {
    console.log(JSON.stringify(req.body));
    try {
        var data = parser.parseReq(req.body, constant.FB_validation.validateFBaccount);
        userController.validateFBaccount(data, function (result) {
            res.statusCode = result.statusCode;
            res.send(result);
        });
    } catch (exception) {
        logger.error('Exception occurs in validateFBaccount ' + exception);
        var responseObj = {};
        responseObj.statusCode = 400;
        responseObj.status = false;
        responseObj.alreadyRegister = false;
        responseObj.message = languageMsg.FB.UNKNOWNERROR
        callback(responseObj);
    }
};


var getTotalFriendCount = function(callback){

}

module.exports = {
    home: home,
    validateFBaccount: validateFBaccount
};