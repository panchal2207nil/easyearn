var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var path = require('path');
var logger = require('../utils/logger.js');
var compression = require('compression');
var router = express.Router();

module.exports = function (app) {
    // view engine setup\
    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'ejs');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(compression()); //use compression
    app.use(function (req, res, next) {
        if (req.headers.origin) {
            res.header('Access-Control-Allow-Origin', '*')
            res.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization')
            res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
        }
        next()
    }); // cross domain allow
    /*router(index);*/
// catch 404 and forward to error handler
    /*app.use(function (req, res, next) {
     logger.info("file not found"+req.query);
     var err = new Error('Not Found');
     err.status = 404;
     next(err);
     });*/

// error handlers
// development error handler
// will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.render('error', {
                message: err.message,
                error: err
            });
        });
    }

// production error handler
// no stack traces leaked to user
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
};