
/*
 *    story no: 267
 *    Dev name: Unnati
 *    modification date: 29 apr 2015
 *    purpose:  parsing each req params
 */

var constant = require('../utils/constant');

exports.parseReq = function(req,type){
    switch (type){
        case constant.FB_validation.validateFBaccount :
            req.body = req.body ? req.body : {};
            req.body.access_token = req.body.access_token ? req.body.access_token : '';
            return req;
            break;
    }
};