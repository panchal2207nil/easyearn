/**
 * Created by inno23 on 17/6/16.
 */
var modal = require('../datamodels/modal');
var languageMsg = require('../utils/languageMsg.js');
var constant = require('../utils/constant');
var createOffer = function (offer, callback) {
    console.log('Offer : ' + JSON.stringify(offer));
    modal.offers.create(offer).then(function (result) {
        if (result) {
            callback(null, result);
            console.log('adminModel createOffer  : ' + JSON.stringify(result));
        } else {
            console.log('adminModel createOffer error : ' + JSON.stringify(result));
            callback(languageMsg.FB.USER_NOT_REGISTERED, result);
        }
    }).catch(function (exception) {
        console.log('adminModel createOffer exit : ' + exception);
        callback(exception, null);
    });
};

var insertOfferActions = function (offerActions, callback) {
    modal.offer_actions.create(offerActions).then(function (result) {
        if (result) {
            callback(null, result);
            console.log('adminModel insertOfferActions  : ' + JSON.stringify(result));
        } else {
            console.log('adminModel insertOfferActions error : ' + JSON.stringify(result));
            callback(languageMsg.FB.USER_NOT_REGISTERED, result);
        }
    }).catch(function (exception) {
        console.log('adminModel insertOfferActions exit : ' + exception);
        callback(exception, null);
    });
};

var getActionDetails = function (actions, callback) {
    //var query = 'SELECT objectID, name FROM  `offer_actions` wHERE  actionID = ' + actions.actionID + ' GROUP BY objectID'
    var query = '';
    if (actions.actionID == constant.ACTIONS.PAGE_LIKE || actions.actionID == constant.ACTIONS.PAGE_SHARE) {
        query = 'SELECT objectID,name FROM `offer_actions` WHERE (`actionID` = ' + constant.ACTIONS.PAGE_LIKE +
            ' or `actionID` = ' + constant.ACTIONS.PAGE_SHARE + ' ) group by objectID'
    } else if (actions.actionID == constant.ACTIONS.POST_LIKE || actions.actionID == constant.ACTIONS.POST_SHARE ||
        actions.actionID == constant.ACTIONS.COMMENT) {
        query = 'SELECT objectID,name FROM `offer_actions` WHERE (`actionID` = ' + constant.ACTIONS.POST_LIKE +
            ' or `actionID` = ' + constant.ACTIONS.POST_SHARE +
            ' or `actionID` = ' + constant.ACTIONS.COMMENT + ' )  group by objectID'
    } else if (actions.actionID == constant.ACTIONS.TWEET_LIKE || actions.actionID == constant.ACTIONS.RE_TWEET) {
        query = 'SELECT objectID,name FROM `offer_actions` WHERE (`actionID` = ' + constant.ACTIONS.TWEET_LIKE +
            ' or `actionID` = ' + constant.ACTIONS.RE_TWEET + ' ) group by objectID'
    } else if (actions.actionID == constant.ACTIONS.FOLLOW) {
        query = 'SELECT objectID,name FROM `offer_actions` WHERE (`actionID` = ' + constant.ACTIONS.FOLLOW
            + ' ) group by objectID'
    }

    modal.sequelize.query(query, {type: modal.sequelize.QueryTypes.SELECT}).then(function (result) {
        if (result) {
            callback(null, result);
            console.log('adminModel getActionDetails  : ' + JSON.stringify(result));
        } else {
            console.log('adminModel getActionDetails error : ' + JSON.stringify(result));
            callback(languageMsg.FB.USER_NOT_REGISTERED, result);
        }
    }).catch(function (exception) {
        console.log('adminModel getActionDetails exit : ' + exception);
        callback(exception, null);
    });
};
module.exports = {
    createOffer: createOffer,
    insertOfferActions: insertOfferActions,
    getActionDetails: getActionDetails
}