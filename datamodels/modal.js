/**
 * New node file
 */
var config = require('../config');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.sqlDatabase, config.sqlUserName, config.sqlPassword, config.mySql);
module.exports.sequelize = sequelize;

var user = sequelize.define('users', {
    userID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    mobileNo: {
        type: Sequelize.STRING
    },
    accessToken: {
        type: Sequelize.STRING
    },
    userType: {
        type: Sequelize.INTEGER
    },
    countryCode: {
        type: Sequelize.STRING
    },
    coins: {
        type: Sequelize.FLOAT,
        defaultValue: 0
    },
    brandName: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    }
}, {
    tableName: 'users',
    freezeTableName: true, timestamps: false
});

module.exports.user = user;

var sources = exports.sources = sequelize.define('sources', {
    sourceID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    is_active: {
        type: Sequelize.INTEGER
    }
}, {
    tableName: 'sources',
    freezeTableName: true, timestamps: false
});

module.exports.sources = sources;


var actions = exports.actions = sequelize.define('actions', {
    actionID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    displayName: {
        type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    sourceID: {
        type: Sequelize.INTEGER
    }
}, {
    tableName: 'actions',
    freezeTableName: true, timestamps: false
});

module.exports.actions = actions;

sources.hasMany(actions, {foreignKey: 'sourceID'});
actions.belongsTo(sources, {foreignKey: 'sourceID'});

var user_sources = exports.user_sources = sequelize.define('user_sources', {
    userSourceID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, sourceID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, socialID: {
        type: Sequelize.STRING
    }, firstName: {
        type: Sequelize.STRING
    }, lastName: {
        type: Sequelize.STRING
    }, emailID: {
        type: Sequelize.STRING
    }, accessToken: {
        type: Sequelize.STRING
    },
    refreshToken: {
        type: Sequelize.STRING
    },
    updateDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    tokenExpiredDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, connectedDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, mobileNo: {
        type: Sequelize.STRING
    },
    access_token_secret: {
        type: Sequelize.STRING
    }
}, {
    tableName: 'user_sources',
    freezeTableName: true, timestamps: false
});

module.exports.user_sources = user_sources;

user.hasMany(user_sources, {foreignKey: 'userID'});
user_sources.belongsTo(user, {foreignKey: 'userID'});

sources.hasMany(user_sources, {foreignKey: 'sourceID'});
user_sources.belongsTo(user, {foreignKey: 'sourceID'});


var offers = exports.offers = sequelize.define('offers', {
    offerID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    }, startDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, endDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, createdDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, updatedDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }, offerImage: {
        type: Sequelize.STRING
    },
    adminID: {
        type: Sequelize.INTEGER
    },
    recommendedText: {
        type: Sequelize.STRING
    },
    postMessage: {
        type: Sequelize.STRING
    }
}, {
    tableName: 'offers',
    freezeTableName: true, timestamps: false
});

module.exports.offers = offers;


var admins = exports.admins = sequelize.define('admins', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    email: {
        type: Sequelize.STRING
    },
    encrypted_password: {
        type: Sequelize.STRING
    },
    reset_password_token: {
        type: Sequelize.STRING
    },
    reset_password_sent_at: {
        type: Sequelize.DATE
    },
    remember_created_at: {
        type: Sequelize.DATE
    },
    sign_in_count: {
        type: Sequelize.INTEGER
    },
    current_sign_in_at: {
        type: Sequelize.DATE
    },
    last_sign_in_at: {
        type: Sequelize.DATE
    }, current_sign_in_ip: {
        type: Sequelize.STRING
    },
    last_sign_in_ip: {
        type: Sequelize.STRING
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    }, provider: {
        type: Sequelize.STRING
    },
    uid: {
        type: Sequelize.STRING
    },
    social_email: {
        type: Sequelize.STRING
    },
    oauth_token: {
        type: Sequelize.STRING
    },
    oauth_expires_at: {
        type: Sequelize.DATE
    },
    coins: {
        type: Sequelize.FLOAT,
        defaultValue: 0
    }
}, {
    tableName: 'admins',
    freezeTableName: true, timestamps: false
})

module.exports.admins = admins;

admins.hasMany(offers, {foreignKey: 'adminID'});
offers.belongsTo(admins, {foreignKey: 'adminID'});

var offer_actions = exports.offer_actions = sequelize.define('offer_actions', {
    offerActionID: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    offerID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, actionID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, sourceID: {
        type: Sequelize.INTEGER
    }, coins: {
        type: Sequelize.FLOAT,
        defaultValue: 0
    },
    pageID: {
        type: Sequelize.STRING
    },
    postID: {
        type: Sequelize.STRING
    },
    public_url: {
        type: Sequelize.STRING
    },
    commentMessage: {
        type: Sequelize.STRING
    }
}, {
    tableName: 'offer_actions',
    freezeTableName: true, timestamps: false
});

module.exports.offer_actions = offer_actions;

offers.hasMany(offer_actions, {foreignKey: 'offerID'});

actions.hasMany(offer_actions, {foreignKey: 'actionID'});
offer_actions.belongsTo(actions, {foreignKey: 'actionID'});

sources.hasMany(offer_actions, {foreignKey: 'sourceID'});
offer_actions.belongsTo(sources, {foreignKey: 'sourceID'});


var user_actions = exports.user_actions = sequelize.define('user_actions', {
    userActionId: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, offerActionID: {
        type: Sequelize.INTEGER,
        allowNull: false
    }, attemptCount: {
        type: Sequelize.INTEGER
    }, actualCount: {
        type: Sequelize.STRING
    }, transactionDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    offerID: {
        type: Sequelize.INTEGER
    },
    actionID: {
        type: Sequelize.INTEGER
    },
    coins: {
        type: Sequelize.FLOAT
    },
    isUnliked: {
        type: Sequelize.INTEGER
    }
}, {
    tableName: 'user_actions',
    freezeTableName: true, timestamps: false
});

module.exports.user_actions = user_actions;

user.hasMany(user_actions, {foreignKey: 'userID'});
user_actions.belongsTo(user, {foreignKey: 'userID'});

offer_actions.hasMany(user_actions, {foreignKey: 'offerActionID'});
user_actions.belongsTo(offer_actions, {foreignKey: 'offerActionID'});


offers.hasMany(user_actions, {foreignKey: 'offerID'});
user_actions.belongsTo(offers, {foreignKey: 'offerID'});

actions.hasMany(user_actions, {foreignKey: 'actionID'});
user_actions.belongsTo(actions, {foreignKey: 'actionID'});


var constant = exports.constant = sequelize.define('constant', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    TotalFriendCount: {
        type: Sequelize.INTEGER
    }
}, {
    tableName: 'constant',
    freezeTableName: true, timestamps: false
});

module.exports.constant = constant;
