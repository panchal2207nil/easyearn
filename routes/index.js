﻿var user = require('../routes/user');
var auth = require("../middleware/authentication");
var FB = require("fb");
module.exports = function (app) {

    /*var token = "CAALr3OUGqTgBAGCzxUjE7aixCuxCKYkhYbMEZCYVMx3qNu0D84a1WS7rCG8ffo14PGUBttju6U8bEKh6BecF5RCCvZC9TYODOLIPaaLK387hKZCowpRNc2ZCbmmcQetEtePM3HFZAcpFZBmCplOq2kwfwhOrprTMytsf5Vi3WIHTHUlNFc82S4Od8ViWxM7tgZD";
    FB.setAccessToken(token)
    var api = {"name": "Aditi Raval","id": "135020493438"};
    FB.api('me/friends', function (facebook_result) {
        console.log(facebook_result);
        callback(facebook_result);
    });*/
    app.get("/", user.home); //home page
    app.post("/test",auth.isAuthorized, user.home); //test api

    // To validate FB account
    app.post('/validateFBaccount',user.validateFBaccount);
};