/**
 * Created by inno41 on 9/2/16.
 */

var sequelize = require('sequelize');
var languageMsg = require('../utils/languageMsg');
var modal = require("../datamodels/modal");
var logger = require('../utils/logger');
var config = require('../config');
var FB = require('fb');
var constants = require('../utils/constant');
var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase(config.neo4j.URL);
var moment = require('moment');
var _ = require('underscore');
var constant = require('../utils/constant');

/**
 * @author Neel
 * @desc to check user exists
 * @param fbID - facebook id
 * @param callback - callback function
 */
var checkUsrExists = function (fbID, callback) {
    logger.info('userModel checkUsrExists enter : ' + fbID);
    modal.user_sources
        .find({where: {socialID: fbID}})
        .then(function (result) {
            if (result) {
                logger.info('userModel checkUsrExists exit : ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.info('userModel checkUsrExists exit');
                callback(languageMsg.FB.USER_NOT_REGISTERED, '');
            }
        })
        .catch(function (exception) {
            logger.info('userModel checkUsrExists exit : ' + exception);
            callback(exception, null);

        });
};


/**
 * @author Harshad Parmar
 * @desc get User details from mobile number
 * @param {Object} data
 *      data.phone(String) - Mobile Number
 *      data.country(String) - Country code
 * @param callback
 */
var getUserFromMobile = function (data, callback) {
    logger.info('userModel getUserFromMobile enter : ' + JSON.stringify(data));
    modal.user
        .findOne({where: {mobileNo: data.phone.trim(), countryCode: data.country}})
        .then(function (result) {
            if (result) {
                callback(null, result);
                logger.info('userModel getUserFromMobile exit : ' + JSON.stringify(result));
            } else {
                callback(languageMsg.FB.USER_NOT_REGISTERED, result);
                logger.info('userModel getUserFromMobile error : ' + JSON.stringify(result));
            }
        })
        .catch(function (exception) {
            callback(exception, null);
            logger.info('userModel getUserFromMobile exit : ' + exception);
        });
};

/**
 * @author Harshad Parmar
 * @desc Create user data in mySQL
 * @date - 12/10/2016
 * @param {Object} data - input data JSON object
 * @param callback - callback function
 */
var insertUserData = function (data, callback) {
    modal.user
        .create(data)
        .then(function (result) {
            if (result) {
                callback(null, result);
                logger.info('userModel insertUserData  : ' + JSON.stringify(result));
            } else {
                logger.info('userModel insertUserData error : ' + JSON.stringify(result));
                callback(languageMsg.FB.USER_NOT_REGISTERED, result);
            }
        })
        .catch(function (exception) {
            logger.info('userModel insertUserData exit : ' + exception);
            callback(exception, null);
        });
};
/**
 * @author Harshad Parmar
 * @desc Extends Facebook token
 * @date - 12/10/2016
 * @param token
 * @param callback
 */
var extendFBToken = function (token, callback) {
    var fb_var = {
        client_id: config.FACEBOOK.CLIENT_ID,
        client_secret: config.FACEBOOK.CLIENT_SECRET,
        grant_type: 'fb_exchange_token',
        fb_exchange_token: token
    };
    FB.api('oauth/access_token', fb_var, function (res) {
        if (!res || res.error) {
            callback();
        } else {
            callback(res);
        }
    });
};
/**
 * @author Harshad Parmar
 * @desc Insert new AccessToken for user
 * @date - 12/10/2016
 * @param data
 * @param callback
 */
var insertAccessToken = function (data, callback) {
    modal.user_sources
        .create(data)
        .then(function (result) {
            if (result) {
                logger.info('userModel insertAccessToken exit : ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.info('userModel insertAccessToken error ');
                callback(languageMsg.FB.USER_NOT_REGISTERED, result);
            }
        })
        .catch(function (exception) {
            logger.error('userModel insertAccessToken exception : ' + exception);
            callback(exception, null);
        });
};
/**
 * @author Harshad Parmar
 * @desc create Facebook user data
 * @date - 12/10/2016
 * @param data
 * @param callback
 */
var createFBUser = function (data, callback) {
    var picture = data.fb.picture ? data.fb.picture.data.url : '';
    var query = 'MERGE (u:User {userID: "' + data.user.userID + '"}) ' +
        ' ON MATCH SET u.fbID="' + data.fb.id + '"' +
        ' ON CREATE SET u.fbID="' + data.fb.id + '"' +
        ' MERGE (fb:Facebook{fbID:"' + data.fb.id + '"})' +
        ' ON MATCH SET fb.firstName="' + data.fb.first_name + '",fb.lastName="' + data.fb.last_name + '",fb.picture="' + picture + '",' +
        ' fb.birthday="' + data.fb.birthday + '", fb.email="' + data.fb.email + '", fb.gender="' + data.fb.gender + '", fb.name="' + data.fb.name + '",' +
        ' fb.relationship_status="' + data.fb.relationship_status + '"' +
        ' ON CREATE SET fb.firstName="' + data.fb.first_name + '",fb.lastName="' + data.fb.last_name + '",fb.picture="' + picture + '",' +
        ' fb.birthday="' + data.fb.birthday + '", fb.email="' + data.fb.email + '", fb.gender="' + data.fb.gender + '", fb.name="' + data.fb.name + '",' +
        ' fb.relationship_status="' + data.fb.relationship_status + '"' +
        ' MERGE (u)-[:CONNECTED]-(fb) return u';
    runNeo4j(query, function (err, results) {
        callback(err, results);
    })
};

/**
 * @author Harshad
 * @param {string} query
 * @param callback
 */
var runNeo4j = function (query, callback) {
    try {
        db.cypher({
            query: query
        }, function (err, results) {
            callback(err, results);
        });
    } catch (error) {
        callback(error, '');
    }
};

/**
 * @author - Neel
 * @modificationDate - 18/02/2016
 * @desc - To get offer list
 * @param {Object} data - input data JSON object
 * @param {array} data.sources - user connected sources
 * @param {Object} data.user - user data
 * @param callback - callback function
 */
var getOfferList = function (data, callback) {

    var sources = data.sources, conditionsArray = [];
    var count = 0, offset = 0;
    /*
     Note : To get only user conneted source offers
     for (var index in sources) {
     conditionsArray.push(" FIND_IN_SET('" + sources[index].sourceID + "',`offers`.`sourceID` ) ");
     }
     var whereCondition = conditionsArray.join(' or ')
     */

    if (data.startIndex == 1) { // if it is start index
        count = constants.LIST_VIEW.START_COUNT;
    } else {
        offset = data.startIndex * constants.LIST_VIEW.OFFSET - constants.LIST_VIEW.START_COUNT;
        count = constants.LIST_VIEW.DEFAULT_COUNT;
    }

    // whereCondition = " ( " + whereCondition + " ) AND ( `offers`.`endDate` >= CURDATE() ) "
    whereCondition = " ( `offers`.`endDate` >= CURDATE() ) "
    console.log('whereCondition ' + whereCondition);
    if (data.offersToRemove.length != 0) {
        whereCondition += ' and offerID NOT IN ( ' + data.offersToRemove.toString() + ' ) ';
    }

    modal.offers.findAll({
        attributes: ['offerID', 'name', 'offerImage', 'recommendedText', 'postMessage'
            , [sequelize.fn('date_format', sequelize.col('startDate'), '%Y-%m-%d %H:%i:%s'), 'startDate']
            , [sequelize.fn('date_format', sequelize.col('endDate'), '%Y-%m-%d %H:%i:%s'), 'endDate']
            , [sequelize.fn('date_format', sequelize.col('createdDate'), '%Y-%m-%d %H:%i:%s'), 'createdDate']
            , [sequelize.fn('date_format', sequelize.col('updatedDate'), '%Y-%m-%d %H:%i:%s'), 'updatedDate']
        ],
        include: [{model: modal.offer_actions, include: [modal.actions, modal.sources], offset: 0, limit: count}],
        where: [whereCondition],
        order: '`offers`.`endDate`',
        limit: count,
        offset: offset

    }).then(function (result) {
        if (result) {
            logger.info('userModel getOfferList result ' + JSON.stringify(result));
            callback(null, result);
        } else {
            logger.info('userModel getOfferList result exit');
            callback(languageMsg.COMMON.USER_NOT_REGISTERED, result);
        }
    }).catch(function (exception) {
        callback(exception, null);
    });
}


/**
 * @author - Neel
 * @modificationDate - 18/02/2016
 * @desc - To get user connected sources
 * @param {string} userID - userID to get user's connected sources
 * @param callback - callback function
 */

var getConnectedSource = function (userID, callback) {
    logger.info('userModel getConnectedSource enter : ' + userID);
    modal.sequelize
        .query('SELECT * FROM  `user_sources` WHERE  `userID` = ' + userID, {type: modal.sequelize.QueryTypes.SELECT})
        .then(function (result) {
            if (result) {
                callback(null, result);
                logger.info('userModel getConnectedSource exit : ' + JSON.stringify(result));
            } else {
                callback(languageMsg.USER.USER_CONNECTED_SOURCE_ERROR, '');
                logger.error('userModel getConnectedSource exit ' + languageMsg.USER.USER_CONNECTED_SOURCE_ERROR);
            }
        })
        .catch(function (exception) {
            callback(exception, null);
            logger.error('userModel getConnectedSource exit : ' + exception);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To get user actions
 * @param {Object} data - input data
 * @param callback - callback function
 */

var getUserAction = function (data, callback) {
    logger.info('userModel getUserAction enter : ');
    modal.user_actions.
        find({where: {offerID: data.offerID, actionID: data.actionID, userID: data.user.userID}})
        .then(function (result) {
            logger.info('userModel getUserAction action : ' + JSON.stringify(result));
            callback(null, result);
        }).catch(function (exception) {
            logger.info('userModel getUserAction exception : ' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To get offer details
 * @param {Object} data - input data
 * @param callback - callback function
 */
var getOffferDetails = function (data, callback) {

    modal.offers.
        findAll({
            include: [modal.offer_actions, modal.admins],
            where: {offerID: data.offerID}
        })
        .then(function (result) {

            if (result) {
                logger.info('userModel getOffferDetails offer : ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.error('userModel getOffferDetails error : ' + JSON.stringify(result));
                callback(languageMsg.OFFER.OFFER_LISTING_ERROR, null);
            }
        }).catch(function (exception) {
            logger.error('userModel getOffferDetails exception : ' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To deduct coins from merchant account
 * @param {Object} data - input data
 * @param callback - callback function
 */
var deduct_coins = function (coins, metchantID, callback) {
    logger.info('userModel deduct_coins enter ');
    var query = 'UPDATE admins SET coins = coins - ' + coins + ' WHERE id = ' + metchantID + '';
    modal.sequelize.query(query).then(function (result) {
        if (result) {
            logger.info('userModel deduct_coins : ' + coins + ' coins deducted in userID : ' + metchantID);
            callback(null, result);
        } else {
            logger.error('userModel deduct_coins error exit : ' + JSON.stringify(result));
            callback(languageMsg.ACTIONS.DEDUCT_COINS_ERROR, null);
        }
    }).catch(function (exception) {
        logger.error('userModel deduct_coins exception' + exception);
        callback(exception, null);
    });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To increse coins in user account
 * @param {Object} data - input data
 * @param callback - callback function
 */

var increase_coins = function (coins, userID, callback) {

    logger.info('userModel increase_coins enter ');
    var query = 'UPDATE users SET coins = coins + ' + coins + ' WHERE userID = ' + userID + '';
    modal.sequelize.query(query).then(function (result) {
        if (result) {
            logger.info('userModel increase_coins : ' + coins + ' coins increased in userID : ' + userID);
            callback(null, result);
        } else {
            logger.error('userModel increase_coins error : ' + JSON.stringify(result));
            callback(languageMsg.ACTIONS.INCREASE_COINS_ERROR, null);
        }
    }).catch(function (exception) {
        logger.error('userModel increase_coins exception' + exception);
        callback(exception, null);
    });
};


/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To add user performed actions
 * @param {Object} data - input data
 * @param callback - callback function
 */

var addUserAction = function (user_action_data, callback) {
    logger.info('userModel addUserAction enter ');
    modal.user_actions
        .create(user_action_data)
        .then(function (result) {
            if (result) {
                logger.info('userModel addUserAction exit : ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.info('userModel addUserAction error exit' + JSON.stringify(result));
                callback(languageMsg.ACTIONS.ADD_USER_ACTION, result);
            }
        })
        .catch(function (exception) {
            logger.error('userModel addUserAction exception : ' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To update user performed actions
 * @param {Object} data - input data
 * @param callback - callback function
 */
var updateUserAction = function (data, callback) {

    logger.info('userModel updateUserAction enter ');
    var user_action_data = {};
    user_action_data.isUnliked = constant.isUnliked.FALSE;
    user_action_data.attemptCount = data.userActionDetails.attemptCount + 1;
    user_action_data.actualCount = data.actualCount;
    user_action_data.transactionDate = moment().format("YYYY-MM-DD HH:mm:ss");
    modal.user_actions
        .update(user_action_data, {where: {offerID: data.offerID, actionID: data.actionID, userID: data.user.userID}})
        .then(function (result) {
            if (result) {
                logger.info('userModel updateUserAction exit : ' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.error('userModel updateUserAction error : ' + JSON.stringify(result));
                callback(languageMsg.ACTIONS.UPDATE_USER_ACTION, result);
            }
        })
        .catch(function (exception) {
            logger.error('userModel updateUserAction exception : ' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc : To get user fb account details
 * @param userID - User id
 * @param callback - callback function
 */
var getUserFbDetails = function (socialID, callback) {

    logger.info('userModel getUserFbDetails enter ');
    modal.user_sources
        .find({where: {socialID: socialID}})
        .then(function (result) {
            if (result) {
                callback(null, result);
                logger.info('userModel getUserFbDetails exit : ' + JSON.stringify(result));
            } else {
                logger.info('userModel getUserFbDetails error exit' + result);
                callback(languageMsg.USER.USER_NOT_FOUND, null);
            }
        })
        .catch(function (exception) {
            logger.error('userModel getUserFbDetails exit : ' + exception);
            callback(exception, null);
        });
};

/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To get user performed actions based on offer id
 * @param {Object} data - input data
 * @param callback - callback function
 */
var getUserPerformedAction = function (data, callback) {

    logger.info('userModel getUserPerformedAction enter');
    var query = "SELECT `userActionId`, `userID`, `offerActionID`, `attemptCount`, `actualCount`," +
        " `transactionDate`, `offerID`, `actionID`, `coins` FROM `user_actions` AS `user_actions` " +
        " WHERE `user_actions`.`offerID` = '" + data.offerID + "' AND `user_actions`.`userID` = '" + data.user.userID + "'"

    modal.sequelize.query(query, {type: modal.sequelize.QueryTypes.SELECT})
        .then(function (result) {
            logger.info('userModel getUserPerformedAction  exit' + result);
            callback(null, result);
        }).catch(function (exception) {
            logger.info('userModel getUserPerformedAction exception' + exception);
            callback(exception, null);
        });
}


/**
 * @author Neel
 * @Modificationdate : 22 Feb 2016
 * @desc - To get user transactions
 * @param {Object} data - input data
 * @param callback - callback function
 */
var getUserTransactions = function (data, callback) {

    var count = 0, offset = 0;
    if (data.startIndex == 1) { // if it is start index
        count = constants.LIST_VIEW.START_COUNT;
    } else {
        offset = data.startIndex * constants.LIST_VIEW.OFFSET - constants.LIST_VIEW.START_COUNT;
        count = constants.LIST_VIEW.DEFAULT_COUNT;
    }

    modal.user_actions.findAll({
        attributes: ['offerID', 'actionID', 'coins', [sequelize.fn('date_format', sequelize.col('transactionDate'), '%d %M %Y'), 'transactionDate']],
        include: [{
            model: modal.offers,
            attributes: ['name', 'offerImage']
        }, {model: modal.actions, attributes: ['actionID', 'name', 'displayName']}],
        where: {userID: data.user.userID, actualCount: 1},
        order: '`user_actions`.`transactionDate` desc',
        limit: count,
        offset: offset
    }).then(function (result) {
        if (result) {
            logger.info('userModel getUserTransactions : ' + JSON.stringify(result));
            callback(null, result);
        } else {
            logger.info('userModel getUserTransactions error : ' + JSON.stringify(result));
            callback(languageMsg.TRANSACTION_LIST.TRANSACTION_LIST_ERROR, null);
        }
    }).catch(function (exception) {
        logger.info('userModel getUserTransactions exception : ' + exception);
        callback(exception, null);
    });
};

/**
 * @author Neel
 * @Modificationdate : 23 Feb 2016
 * @desc - To get user performed action offers
 * @param {Object} data - input data
 * @param callback - callback function
 */

var getUserOffers = function (data, callback) {

    modal.user_actions.findAll({
        where: {userID: data.user.userID},
        include: [{model: modal.offers, include: [{model: modal.offer_actions}]}]
    }).then(function (result) {
        if (result) {
            logger.info('userModel getUserOffers : ' + JSON.stringify(result));
            callback(null, result);
        } else {
            logger.error('userModel getUserOffers error: ' + JSON.stringify(result));
            callback(languageMsg.OFFER.OFFER_LISTING_ERROR, null);
        }
    }).catch(function (exception) {
        console.log(exception);
        logger.error('userModel getUserOffers exception: ' + exception);
        callback(exception, null);
    });
};


/**
 * @author Neel
 * @Modificationdate : 25 Feb 2016
 * @desc - To get user data
 * @param {Object} data - input data
 * @param callback - callback function
 */

var getUserData = function (data, callback) {

    modal.user.find({where: {userID: data.user.userID}}).then(function (result) {
        if (result) {
            logger.info('userModel getCoins : ' + JSON.stringify(result));
            callback(null, result);
        } else {
            logger.error('userModel getCoins error: ' + JSON.stringify(result));
            callback(languageMsg.OFFER.OFFER_LISTING_ERROR, null);
        }
    }).catch(function (exception) {
        logger.error('userModel getCoins exception: ' + exception);
        callback(exception, null);
    });
};


var getUserLikedPages = function (fbID, callback) {
    logger.info('userModel getUserDetails enter : ' + fbID);

    var query = 'SELECT user_actions.isUnliked as isUnliked, user_actions.offerID as offerID,user_actions.actionID as actionID, user_sources.userID as userID, offers.userID as merchantID ,' +
        ' offer_actions.objectID as objectID, user_sources.accessToken as accessToken ,offer_actions.coins as coins FROM  ' +
        '`user_sources` JOIN  `user_actions` ON user_sources.userID = user_actions.userID ' +
        ' JOIN  `offer_actions` ON user_actions.offerActionID = offer_actions.offerActionID ' +
        ' JOIN  `offers` ON offer_actions.offerID = offers.offerID ' +
        ' WHERE  user_sources.socialID = ' + fbID + ' and user_actions.actionID = ' + constants.ACTIONS.PAGE_LIKE +
        ' and user_actions.actualCount = ' + constants.ACTION_STATUS.COMPLETED

    modal.sequelize.query(query, {type: modal.sequelize.QueryTypes.SELECT}).
        then(function (result) {
            if (result) {
                logger.info('userModel getUserDetails exit : Successfully get user likes');
                callback(null, result);
            } else {
                logger.info('userModel getUserDetails exit : Error occur while getting user liked pages');
                callback(languageMsg.USER.PAGE_LIKE_ERROR, '');
            }
        })
        .catch(function (exception) {
            logger.info('userModel getUserDetails exit : ' + exception);
            callback(exception, null);
        });
};


var updateOfferActionStatus = function (data, callback) {

    modal.user_actions.update({actualCount: constants.ACTION_STATUS.INCOMPLETE, isUnliked: constants.isUnliked.TRUE}, {
        where: {userID: data.userID, offerID: data.offerID, actionID: data.actionID}
    }).then(function (result) {
        if (result) {
            logger.info('userModel updateOfferActionStatus : Offer action status changed successfully ');
            callback(null, result);
        } else {
            logger.error('userModel updateOfferActionStatus : Error occur while updating offer action ' + JSON.stringify(result));
            callback(languageMsg.ACTIONS.STATUS_CHAGE_ERROR, null);
        }
    }).catch(function (exception) {
        logger.info('userModel updateOfferActionStatus exception : ' + exception);
        callback(exception, null);
    });
};


var getUserTwitterActions = function (callback) {
    logger.info('userModel getUserTwitterActions enter ');

    var twitterActions = '' + constant.ACTIONS.TWEET_LIKE + ',' + constant.ACTIONS.RE_TWEET + ',' + constant.ACTIONS.FOLLOW + ''
    var query = 'SELECT user_sources.socialID ,user_actions.isUnliked as isUnliked, user_actions.offerID as offerID,' +
        'user_actions.actionID as actionID, user_sources.userID as userID, offers.adminID as merchantID ,' +
        ' offer_actions.objectID as objectID, user_sources.accessToken as accessToken , ' +
        'user_sources.access_token_secret as access_token_secret,offer_actions.coins as coins ' +
        'FROM`user_sources` JOIN  `user_actions` ON user_sources.userID = user_actions.userID ' +
        ' JOIN  `offer_actions` ON user_actions.offerActionID = offer_actions.offerActionID ' +
        ' JOIN  `offers` ON offer_actions.offerID = offers.offerID WHERE user_sources.sourceID = ' + constant.SOURCE.TWITTER +
        ' and user_actions.actualCount = ' + constants.ACTION_STATUS.COMPLETED +
        " and FIND_IN_SET( user_actions.actionID, '" + twitterActions + "')"
    modal.sequelize.query(query, {type: modal.sequelize.QueryTypes.SELECT}).
        then(function (result) {
            if (result) {
                logger.info('userModel getUserTwitterActions exit : Successfully get user twitter actions' + JSON.stringify(result));
                callback(null, result);
            } else {
                logger.info('userModel getUserTwitterActions exit : Error occur while getting user twitter actions');
                callback(languageMsg.ACTIONS.GET_ACTION_ERROR, '');
            }
        })
        .catch(function (exception) {
            logger.info('userModel getUserTwitterActions exit : ' + exception);
            callback(exception, null);
        });
};


/**
 * @author - Neel
 * @modificationDate - 30/05/20016
 * @desc - To get user facebook token
 * @param {string} userID - userID to get user's connected sources
 * @param callback - callback function
 */

var getUserFbData = function (userID, callback) {
    logger.info('userModel getUserFbToken enter : ' + userID);
    var query = 'SELECT * FROM  user_sources WHERE  `userID` = ' + userID + ' and sourceID = ' + constant.SOURCE.FACEBOOK;
    modal.sequelize
        .query(query, {type: modal.sequelize.QueryTypes.SELECT})
        .then(function (result) {
            if (result) {
                callback(null, result);
                logger.info('userModel getUserFbToken exit : ' + JSON.stringify(result));
            } else {
                callback(languageMsg.USER.USER_CONNECTED_SOURCE_ERROR, '');
                logger.error('userModel getUserFbToken exit ' + languageMsg.USER.GET_FB_ACCESSTOKEN_ERROR);
            }
        })
        .catch(function (exception) {
            callback(exception, null);
            logger.error('userModel getUserFbToken exit : ' + exception);
        });
};


var addGroups = function (query, callback) {
    runNeo4j(query, function (err, results) {
        callback(err, results);
    })
};


var getUserGroups = function (query, callback) {
    runNeo4j(query, function (err, results) {
        callback(err, results);
    })
};

module.exports = {
    checkUsrExists: checkUsrExists,
    getUserFromMobile: getUserFromMobile,
    insertUserData: insertUserData,
    extendFBToken: extendFBToken,
    insertAccessToken: insertAccessToken,
    createFBUser: createFBUser,
    getOfferList: getOfferList,
    getConnectedSource: getConnectedSource,
    getUserAction: getUserAction,
    getOffferDetails: getOffferDetails,
    deduct_coins: deduct_coins,
    increase_coins: increase_coins,
    addUserAction: addUserAction,
    updateUserAction: updateUserAction,
    getUserFbDetails: getUserFbDetails,
    getUserPerformedAction: getUserPerformedAction,
    getUserTransactions: getUserTransactions,
    getUserOffers: getUserOffers,
    getUserData: getUserData,
    getUserLikedPages: getUserLikedPages,
    updateOfferActionStatus: updateOfferActionStatus,
    getUserTwitterActions: getUserTwitterActions,
    getUserFbData: getUserFbData,
    addGroups: addGroups,
    getUserGroups: getUserGroups
};