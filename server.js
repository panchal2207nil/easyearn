var express = require('express');
var middleWares = require('./middleware/index.js');
var util = require('util');
var app = express();
var logger = require('./utils/logger.js');
var config = require('./config.json');
var routes = require('./routes/index');
middleWares(app);
routes(app);
app.set('port', config.port); //port for environment variable
app.set('env', config.env); //configure environment
// start HTTP server
app.listen(app.get('port'), function () {
    console.log(util.format('http server with pid:%s listening on port:%s', process.pid, app.get('port')));
    logger.info(util.format('api http server with pid:%s listening on port:%s', process.pid, app.get('port')));
    logger.info(util.format('Environment:%s', config.env));
});
module.exports = app;
